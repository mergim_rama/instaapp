package com.mergimrama.instaapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.mergimrama.instaapp.UIActivity.ListAdapter;
import com.mergimrama.instaapp.UIActivity.PostFeedActivity;
import com.mergimrama.instaapp.callbacks.PostsCallback;
import com.mergimrama.instaapp.model.Posts;
import com.mergimrama.instaapp.model.User;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ListAdapter listAdapter;
    public static String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);
        listAdapter = new ListAdapter(getLayoutInflater(), this);
        listView.setAdapter(listAdapter);
        userId = getIntent().getStringExtra("userId");


    }

    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add_button) {
            Intent intent = new Intent(MainActivity.this, PostFeedActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
